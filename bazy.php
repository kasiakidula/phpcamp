<?php

interface iDB {
    /**
     * @param string $host Adres DB
     * @param string $login Login u�ytkownika
     * @param string $password Has�o u�ytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName);

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy si� uda�o wykona� zapytanie czy nie
     */
    public function query($query);

    /**
     * Zwraca liczb� wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows();

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow();

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows();
}


class DB implements iDB {
   $host;
   $login; 
   $password;
   $dbName;
   $handle_db;
   $handle_query;
   
    public function __construct($host, $login, $password, $dbName){
    $this->host=$host;
    $this->login=$login;
    $this->password=$password;
    $this->dbName=$dbName;
    $this->handle_db=new mysqli_connect($host, $dbName, $login, $pasword);
    }

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * @param string $query Zapytanie do wykonania
     * @return bool Czy si� uda�o wykona� zapytanie czy nie
     */
    public function query($query){
    $this->handle_query=mysqli_query($handle_db,$query);
    if($this->handle_query){
      return true;
    }
    else {
      return false;
    }
    }

    /**
     * Zwraca liczb� wierszy zmodyfikowanych przez ostatnie zapytanie
     * @return int
     */
    public function getAffectedRows(){
    $countrows=$this->handle_query->affected_rows;
    return "Zmodyfikowana liczba wierszy: ". $countrows;
    }

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getRow(){
    $row=mysqli_fetch_row($this->handle_query);
    return $row;
    
    }

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * @return mixed
     */
    public function getAllRows(){
    $allrows=mysqli_fetch_all($this->handle_query);
    return $allrow;
    
    }


}

?>